/*********************************************************************************************/

【*】 程序简介 
-工程名称：位带操作
-实验平台: 野火STM32 MINI 开发板
-MDK版本：5.16
-ST固件库版本：3.5.0


【 ！】功能简介：
GPIO 位带操作，配合延时函数实现led灯闪烁


【 ！！】注意事项：
无


/*********************************************************************************************/

【*】 引脚分配

LED：
LED灯的两个阴极接到STM32两个个引脚上，LED低电平亮。

	LED<--->PC2
	LED<--->PC3

	
/*********************************************************************************************/

【*】 时钟

A.晶振：
-外部高速晶振：8MHz
-RTC晶振：32.768KHz

B.各总线运行时钟：
-系统时钟 = SYCCLK = AHB1= PLLCLK= 72MHz
-APB2 =PCLK2 = HCLK = 72MHz 
-APB1 =PCLK1 = HCLK/2 = 36MHz

C.浮点运算单元：
  不使用


/*********************************************************************************************/

【*】 联系我们

-野火官网  :https://embedfire.com
-野火论坛  :http://www.firebbs.cn
-野火商城  :https://yehuosm.tmall.com/
-野火资料下载中心：http://doc.embedfire.com/products/link

/*********************************************************************************************/