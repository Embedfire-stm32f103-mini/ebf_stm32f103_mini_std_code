/*********************************************************************************************/
本文档使用 TAB = 4 对齐，使用keil5默认配置打开阅读比较方便。

【*】程序简介

-工程名称：液晶显示中英文（字库在SD卡）
-实验平台: 野火STM32 MINI 开发板 
-MDK版本：5.16



【 ！】功能简介：
使用SD卡中的字模显示汉字

学习目的：学习如何使用SD卡字模库显示汉字。


【 ！】实验操作：
测试前，给开发板插入32G以内的SD卡，SD卡内要求有字库文件，位置： 

			根目录：/srcdata/GB2312_H1616.FON
			
连接好配套的3.2_2.8寸液晶屏，下载程序后复位开发板即可，屏幕会绘制文字及图形。
若汉字不正常显示，请检查字模文件路径是否正确，SD卡是否能被STM32识别

【*】注意事项：
本程序液晶显示的汉字字模是存储在SD卡文件系统中的，显示速度比FLASH字模慢很多。
字模：GB2312汉字库，16*16，宋体，支持中文标点。

可通过 fonts.h文件中的宏GBKCODE_FLASH 来选择使用FLASH字模还是SD卡字模。
2.8寸 屏幕芯片：ILI9341
3.2寸 屏幕芯片：ST7789V（现在生产使用）
开发板配套程序里已做兼容ILI9341和ST7789V（例程中的 LCD 函数为了与视频教材保持一致仍以 ILI9341 为前缀命名，用户在实际使用时可以根据自己需要来命名）
开发板采用的主控芯片型号为 STM32RCT6，不具备 FSMC 功能，所以只能采用类似软件模拟 I2C 时序那样，模拟 8080 的时序来驱动液晶屏。
程序禁止了 JTAG 功能，所以在使用下载器下载程序时，应把下载接口选择成 SWD 模式

白屏条纹问题：
若是发现显示白屏的时候屏幕有明显条纹，可更改液晶屏内部的 Power control A (CBh) 寄存器的第5个参数，
将该参数的值改到最大0x06，就不会有白屏条纹了。程序里已默认改为最大0x06。

/*********************************************************************************************/

【*】 引脚分配

液晶屏：
使用模拟时序产生8080时序与液晶屏驱动芯片ILI9341通讯.


		/*液晶控制信号线*/
		ILI9341_CS 	<--->	PC4      	//片选
		ILI9341_DC  <---> 	PC7			//选择输出命令还是数据
		ILI9341_WR 	<---> 	PC6			//写使能
		ILI9341_RD  <---> 	PC5			//读使能
		ILI9341_RST	<---> 	NRST		//STM32的复位引脚，上电同时复位液晶
		ILI9341_BK 	<---> 	PD2 		//背光引脚
		
		ILI9341_D0~D15 <---> PB0~PB15
		
	本实验没有驱动触摸屏，详看触摸画板实验。


(microSD卡座，支持32G以内的SD卡)：
microtSD卡座的接口与STM32的SPI相连。

		(CLK)SCK	<--->PA5
		(DATA0)MISO	<--->PA6
		(CMD)MOSI	<--->PA7
		(DATA3)CS	<--->PA8

串口(TTL-USB TO USART)：
CH340的收发引脚与STM32的发收引脚相连。
	CH340  RXD  <----->  USART1  TX  (PA9)
	CH340  TXD  <----->  USART1  RX  (PA10)	
									
/*********************************************************************************************/

【*】 程序描述

	<fonts.c>
	
		1.定义一系列大小为8x16、16x24、24x32的ASCII码表的字模数据
		2.选择使用FLASH字模还是SD卡的字模
		3.获取FLASH中GB2312字模和旧版的HZLIB字模的函数定义
		4.获取SD卡中文显示字库数据函数定义
	
    对于不同的 MCU 屏幕芯片以上的教程流程都适用，一般差异在于配置参数不同，同型号芯片也
    可能因为厂商不同的批次需要调整部分配置参数。
    现程序兼体做法：
    LCD 初始化函数 ILI9341_REG_Config() 里先用 ILI9341_ReadID() 读取 LCD 控制芯片的 ID，并
    使用一个变量 lcdid 来保存它，然后判断对比此时实际屏幕的型号从而执行不同的配置参数过程，
    配置参数一般也是由屏幕厂商提供，有需要时再微调。
    另外有一个比较重要的函数是 ILI9341_GramScan()，在这个函数里面我们也根据变量 lcdid 做了
    不同的配置，比如配置 LCD 显示的颜色模式。
    
/*********************************************************************************************/

【*】 版本

-程序版本：1.0
-发布日期：2013-10

/*********************************************************************************************/

【*】 联系我们

-野火官网  :https://embedfire.com
-野火论坛  :http://www.firebbs.cn
-野火商城  :https://yehuosm.tmall.com
-野火资料下载中心 :http://doc.embedfire.com/products/link

/*********************************************************************************************/